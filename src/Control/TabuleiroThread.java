package Control;

import Model.Tabuleiro;

public class TabuleiroThread extends Thread{
	private Tabuleiro tabuleiro;
	private boolean running = true;
	
	public TabuleiroThread(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}
	
	@Override
	public void run() {
		while(running) {
			try {
				Thread.sleep(100);
			}
			catch (InterruptedException e) {
				System.out.println(e);
			}
			tabuleiro.paint(tabuleiro.getGraphics());
		}
	}
}
