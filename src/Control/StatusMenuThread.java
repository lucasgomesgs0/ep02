package Control;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import Model.Jogador;
import Model.Tabuleiro;
import View.GameOverDialog;
import View.WinDialog;

public class StatusMenuThread extends Thread{
	private Jogador jogador;
	private JPanel status;
	private boolean running = true;
	
	public StatusMenuThread(Jogador jogador,JPanel status) {
		this.jogador = jogador;
		this.status = status;
	}
	
	@Override
	public void run() {
		JLabel texto = new JLabel();
		JLabel embarcacoes = new JLabel();
		
		while(running) {
			
			try {
				Thread.sleep(1);
			}
			catch (InterruptedException e) {
				System.out.println(e);
			}
			
//			texto.setFont(new Font("Dialog", Font.BOLD, 20));
			texto.setText("Chances: "+Integer.toString(jogador.getChances()));
			status.add(texto);
			
//			embarcacoes.setFont(new Font("Dialog", Font.BOLD, 20));
			embarcacoes.setText("Embarcacoes: "+Tabuleiro.getTotalEmbarcacoes());
			status.add(embarcacoes);
			
			
			if (jogador.getChances() == 0) {
				jogador.setChances(-1);
				GameOverDialog gameOverDialog = new GameOverDialog(Tabuleiro.getMainFrame());
				gameOverDialog.setVisible(true);
			}
			else if(Tabuleiro.getTotalEmbarcacoes()==0) {
				Tabuleiro.setTotalEmbarcacoes(-1);
				WinDialog winDialog = new WinDialog(Tabuleiro.getMainFrame());
				winDialog.setVisible(true);
			}
		}
	}
}
