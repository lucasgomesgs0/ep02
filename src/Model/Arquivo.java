package Model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Arquivo {
	private int colunas;
	private int linhas;
	private int[][] matriz;
	private Map<Integer, Integer> embarcacoes;
	private BufferedReader inputTxt;

	public Arquivo() {
		embarcacoes = new HashMap<>();
	}

	public int getColunas() {
		return colunas;
	}

	public void setColunas(int colunas) {
		this.colunas = colunas;
	}

	public int getLinhas() {
		return linhas;
	}

	public void setLinhas(int linhas) {
		this.linhas = linhas;
	}

	public int[][] getMatriz() {
		return matriz;
	}

	public int getMatriz(int i, int j) {
		return matriz[i][j];
	}

	public void setMatriz(int i, int j, int valor) {
		this.matriz[i][j] = valor;
	}

	public void setMatriz(int[][] matriz) {
		this.matriz = matriz;
	}

	public Map<Integer, Integer> getEmbarcacoes() {
		return embarcacoes;
	}

	public void setEmbarcacoes(int key, int value) {
		this.embarcacoes.put(key, value);
	}

	public void setEmbarcacoes(Map<Integer, Integer> embarcacoes) {
		this.embarcacoes = embarcacoes;
	}

	public void lerArquivo(String nomeArquivo) {
		String path = System.getProperty("user.dir") + "/maps/";
		String buffer = null;
		try {
			inputTxt = new BufferedReader(new FileReader(path + nomeArquivo));

			inputTxt.readLine();

			buffer = inputTxt.readLine();
			setColunas(Integer.parseInt(buffer.substring(0, buffer.indexOf(' '))));
			setLinhas(Integer.parseInt(buffer.substring(buffer.indexOf(' ') + 1, buffer.length())));
			matriz = new int[getLinhas()][getColunas()];

			inputTxt.readLine();
			inputTxt.readLine();

			for (int i = 0; i < linhas; i++) {
				for (int j = 0; j < colunas; j++) {
					setMatriz(i, j, Integer.parseInt(String.valueOf((char) inputTxt.read())));
				}
				inputTxt.read();
			}

			inputTxt.readLine();
			inputTxt.readLine();

			for (int i = 0; i < 5; i++) {
				buffer = inputTxt.readLine();
				setEmbarcacoes(i + 1, Integer.parseInt(buffer.substring(buffer.indexOf(' ') + 1, buffer.length())));
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (inputTxt != null) {
				try {
					inputTxt.close();
				} catch (IOException e) {
					System.out.println(e);
				}
			}

		}
	}
}
