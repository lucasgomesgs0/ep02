package Model;

public class Jogador {
	private int chances;

	public int getChances() {
		return chances;
	}

	public void setChances(int chances) {
		this.chances = chances;
	}

	public int jogadaNormal() {
		chances--;
		return chances;
	}
	
	public int jogadaEspecialLinha() {
		chances-=8;
		return chances;
	}
	public int jogadaEspecialColuna() {
		chances-=8;
		return chances;
	}
	public int jogadaEspecialArea2x2() {
		chances-=6;
		return chances;
	}
	public int jogadaDescobrirArea2x2() {
		chances-=3;
		return chances;
	}
}
