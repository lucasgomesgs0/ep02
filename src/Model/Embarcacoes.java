package Model;


public class Embarcacoes {
	private int embarcacao;
	private int[] posicaoInicial;
	private boolean direcaoHorizontal;
	private int qtdAcertosNaEmbarcacao;
	private boolean jaPassouCasa;
	private boolean afundou;
	
	public Embarcacoes() {
		this.posicaoInicial = new int[2];
		this.direcaoHorizontal = false;
		this.jaPassouCasa = false;
		this.afundou = false;
		this.qtdAcertosNaEmbarcacao=0;
	}
	
	public int[] getPosicaoInicial() {
		return posicaoInicial;
	}
	public void setPosicaoInicial(int x, int y) {
		this.posicaoInicial[0] = x;
		this.posicaoInicial[1] = y;
	}
	public int getEmbarcacao() {
		return embarcacao;
	}
	public void setEmbarcacao(int embarcacao) {
		this.embarcacao = embarcacao;
	}
	public boolean isDirecaoHorizontal() {
		return direcaoHorizontal;
	}
	public void setDirecaoHorizontal(boolean direcaoHorizontal) {
		this.direcaoHorizontal = direcaoHorizontal;
	}
	public int getQtdAcertosNaEmbarcacao() {
		return qtdAcertosNaEmbarcacao;
	}
	
	public void setQtdAcertosNaEmbarcacao(int qtdAcertosNaEmbarcacao) {
		this.qtdAcertosNaEmbarcacao = qtdAcertosNaEmbarcacao;
	}
	public void setQtdAcertosNaEmbarcacao() {
		this.qtdAcertosNaEmbarcacao++;
	}
	public boolean isJaPassouCasa() {
		return jaPassouCasa;
	}
	public void setJaPassouCasa(boolean jaPassouCasa) {
		this.jaPassouCasa = jaPassouCasa;
	}

	public boolean isAfundou() {
		return afundou;
	}

	public void setAfundou(boolean afundou) {
		this.afundou = afundou;
	}

}
