package Model;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import View.DescobrirAreaDialog;

public class Tabuleiro extends Canvas {
	private Arquivo arquivo;
	private Jogador jogador;
	private int quadradoWidth;
	private int quadradoHeight;
	private int canvasColunas;
	private int canvasLinhas;
	private boolean[][] matrizExplosao;
	public Embarcacoes[][] embarcacoes;
	private static int totalEmbarcacoes;
	private static JFrame mainFrame;
	public static int jogada = 0;

	public Tabuleiro(Arquivo arquivo, Jogador jogador) {
		this.arquivo = arquivo;
		this.jogador = jogador;
		totalEmbarcacoes = 0;
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		setCanvasColunas(arquivo.getColunas());
		setCanvasLinhas(arquivo.getLinhas());

		matrizExplosao = new boolean[getCanvasColunas() + 1][getCanvasLinhas() + 1];
		embarcacoes = new Embarcacoes[getCanvasLinhas()][getCanvasColunas()];
		setEmbarcacoes();

		setQuadradoWidth((screenSize.width - 290) / (getCanvasColunas() + 1));
		setQuadradoHeight((screenSize.height - 55) / (getCanvasLinhas() + 1));

		for (int i = 1; i <= 5; i++) {
			totalEmbarcacoes += arquivo.getEmbarcacoes().get(i);
		}
	}

	@Override
	public void paint(Graphics g) {
		// Prepare an ImageIcon
		ImageIcon icon = new ImageIcon("images/onda.jpg");
		ImageIcon iconAcertou = new ImageIcon("images/ondaAcertou.jpg");
		ImageIcon iconShot = new ImageIcon("images/explosion.png");

		// ImageIcon Ships
		ImageIcon submarineIcon = new ImageIcon("images/ships/1Submarine.png");
		ImageIcon patrolBoatIcon = new ImageIcon("images/ships/2PatrolBoat.png");
		ImageIcon destroyerIcon = new ImageIcon("images/ships/3Destroyer.png");
		ImageIcon cruiserIcon = new ImageIcon("images/ships/4Cruiser.png");
		ImageIcon battleShipIcon = new ImageIcon("images/ships/5Battleship.png");

		// Rotate ImageIcon Ships
		ImageIcon patrolBoatIconRotate = new ImageIcon("images/ships/2PatrolBoatRotate.png");
		ImageIcon destroyerIconRotate = new ImageIcon("images/ships/3DestroyerRotate.png");
		ImageIcon cruiserIconRotate = new ImageIcon("images/ships/4CruiserRotate.png");
		ImageIcon battleShipIconRotate = new ImageIcon("images/ships/5BattleshipRotate.png");

		// Prepare an Image object to be used by drawImage()
		final Image img = icon.getImage();
		final Image imgAcertou = iconAcertou.getImage();
		final Image imgShot = iconShot.getImage();

		// Image object ships
		final Image submarine = submarineIcon.getImage();
		final Image patrolBoat = patrolBoatIcon.getImage();
		final Image destroyer = destroyerIcon.getImage();
		final Image cruiser = cruiserIcon.getImage();
		final Image battleShip = battleShipIcon.getImage();

		// Rotate image
		final Image patrolBoatRotate = patrolBoatIconRotate.getImage();
		final Image destroyerRotate = destroyerIconRotate.getImage();
		final Image cruiserRotate = cruiserIconRotate.getImage();
		final Image battleShipRotate = battleShipIconRotate.getImage();

		// Positions
		int x = 0;
		int y = 0;

		g.setFont(new Font("Arial", 1, 18));
		for (int i = 0; i < canvasColunas + 1; i++) {
			for (int j = 0; j < canvasLinhas + 1; j++) {
				if (i == 0 && j > 0) {
					g.drawString(Character.toString((char) (64 + j)), i * quadradoWidth + quadradoWidth / 2,
							(j + 1) * quadradoHeight - quadradoHeight / 2);
					g.drawRect(i * quadradoWidth, (j + 1) * quadradoHeight - quadradoHeight, quadradoWidth,
							quadradoHeight);
				}

				else if (j == 0 && i > 0) {
					g.drawString(String.valueOf(i), i * quadradoWidth + quadradoWidth / 2,
							(j + 1) * quadradoHeight - quadradoHeight / 2);
					g.drawRect(i * quadradoWidth, (j + 1) * quadradoHeight - quadradoHeight, quadradoWidth,
							quadradoHeight);
				} else if (j > 0 && i > 0) {
					g.drawImage(img, i * quadradoWidth, j * quadradoHeight, quadradoWidth, quadradoHeight, null);
					g.drawRect(i * quadradoWidth, (j + 1) * quadradoHeight - quadradoHeight, quadradoWidth,
							quadradoHeight);

				}

				if (matrizExplosao[i][j]) {
					x = this.embarcacoes[j-1][i-1].getPosicaoInicial()[0];
					y = this.embarcacoes[j-1][i-1].getPosicaoInicial()[1];

					switch (this.embarcacoes[j-1][i-1].getEmbarcacao()) {
					case 1:
						g.drawImage(submarine, i * quadradoWidth + 25, j * quadradoHeight + 2, 35, quadradoHeight - 2,
								null);
						if (!this.embarcacoes[x][y].isAfundou()) {
							this.embarcacoes[x][y].setAfundou(true);
							totalEmbarcacoes--;
						}
						break;

					case 2:
						if (this.embarcacoes[x][y].getQtdAcertosNaEmbarcacao() == 2) {
							if (this.embarcacoes[x][y].isDirecaoHorizontal()) {
								g.drawImage(patrolBoatRotate, (y + 1) * quadradoWidth + 2,
										(x + 1) * quadradoHeight + 25, quadradoWidth * 2 - 2, 35, null);
							} else {
								g.drawImage(patrolBoat, (y + 1) * quadradoWidth + 20, (x + 1) * quadradoHeight + 2, 35,
										quadradoHeight * 2 - 2, null);
							}
							if (!this.embarcacoes[x][y].isAfundou()) {
								this.embarcacoes[x][y].setAfundou(true);
								totalEmbarcacoes--;
							}
						} else
							g.drawImage(imgAcertou, i * quadradoWidth, j * quadradoHeight, quadradoWidth,
									quadradoHeight, null);
						break;

					case 3:
						if (this.embarcacoes[x][y].getQtdAcertosNaEmbarcacao() == 3) {
							if (this.embarcacoes[x][y].isDirecaoHorizontal()) {
								g.drawImage(destroyerRotate, (y + 1) * quadradoWidth + 2, (x + 1) * quadradoHeight + 25,
										quadradoWidth * 3 - 2, 35, null);
							} else {
								g.drawImage(destroyer, (y + 1) * quadradoWidth + 25, (x + 1) * quadradoHeight + 2, 35,
										quadradoHeight * 3 - 2, null);
							}
							if (!this.embarcacoes[x][y].isAfundou()) {
								this.embarcacoes[x][y].setAfundou(true);
								totalEmbarcacoes--;
							}
						} else
							g.drawImage(imgAcertou, i * quadradoWidth, j * quadradoHeight, quadradoWidth,
									quadradoHeight, null);
						break;

					case 4:
						if (this.embarcacoes[x][y].getQtdAcertosNaEmbarcacao() == 4) {
							if (this.embarcacoes[x][y].isDirecaoHorizontal()) {
								g.drawImage(cruiserRotate, (y + 1) * quadradoWidth + 2, (x + 1) * quadradoHeight + 25,
										quadradoWidth * 4 - 2, 35, null);
							} else {
								g.drawImage(cruiser, (y + 1) * quadradoWidth + 25, (x + 1) * quadradoHeight + 2, 35,
										quadradoHeight * 4 - 2, null);
							}
							if (!this.embarcacoes[x][y].isAfundou()) {
								this.embarcacoes[x][y].setAfundou(true);
								totalEmbarcacoes--;
							}
						} else
							g.drawImage(imgAcertou, i * quadradoWidth, j * quadradoHeight, quadradoWidth,
									quadradoHeight, null);
						break;

					case 5:

						if (this.embarcacoes[x][y].getQtdAcertosNaEmbarcacao() == 5) {
							if (this.embarcacoes[x][y].isDirecaoHorizontal()) {
								g.drawImage(battleShipRotate, (y + 1) * quadradoWidth + 2,
										(x + 1) * quadradoHeight + 25, quadradoWidth * 5 - 2, 35, null);
							} else {
								g.drawImage(battleShip, (y + 1) * quadradoWidth + 25, (x + 1) * quadradoHeight + 2, 35,
										quadradoHeight * 5 - 2, null);
							}
							if (!this.embarcacoes[x][y].isAfundou()) {
								this.embarcacoes[x][y].setAfundou(true);
								totalEmbarcacoes--;
							}
						} else
							g.drawImage(imgAcertou, i * quadradoWidth, j * quadradoHeight, quadradoWidth,
									quadradoHeight, null);
						break;

					default:
						g.drawImage(imgShot, i * quadradoWidth, j * quadradoHeight, quadradoWidth, quadradoHeight,
								null);
						break;
					}
				}
			}
		}

	}

	public static JFrame getMainFrame() {
		return mainFrame;
	}

	public void setMainFrame(JFrame mainFrame) {
		Tabuleiro.mainFrame = mainFrame;
	}

	public void setShot(int x, int y) {
		if (!matrizExplosao[x][y] && jogada!=0) {
			
			
			if (jogada == 1) {
				int xPosInicial = this.embarcacoes[y - 1][x - 1].getPosicaoInicial()[0];
				int yPosInicial = this.embarcacoes[y - 1][x - 1].getPosicaoInicial()[1];
				
				matrizExplosao[x][y] = true;
				jogador.jogadaNormal();
				this.embarcacoes[xPosInicial][yPosInicial].setQtdAcertosNaEmbarcacao();
			} else if (jogada == 2) {
				jogador.jogadaEspecialLinha();
				for(int i=0;i<canvasColunas;i++) {
					matrizExplosao[i+1][y] = true;
					this.embarcacoes[y-1][i].setQtdAcertosNaEmbarcacao();
				}
				
			} else if (jogada == 3) {
				jogador.jogadaEspecialColuna();
				for(int i=0;i<canvasLinhas;i++) {
					matrizExplosao[x][i+1] = true;
					this.embarcacoes[i][x-1].setQtdAcertosNaEmbarcacao();
				}
			} else if (jogada == 4) {
				jogador.jogadaEspecialArea2x2();
				
				if(x<canvasColunas && y < canvasLinhas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							matrizExplosao[x+i][y+j] = true;
						}
					}
				}
				else if(x==canvasColunas && y==canvasLinhas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							matrizExplosao[x-i][y-j] = true;
						}
					}
				}
				else if(y==canvasLinhas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							matrizExplosao[x+i][y-j] = true;
						}
					}
				}
				else if(x==canvasColunas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							matrizExplosao[x-i][y+j] = true;
						}
					}
				}
			} else if (jogada == 5) {
				jogador.jogadaDescobrirArea2x2();
				boolean possui=false;
				DescobrirAreaDialog descobrirAreaDialog = new DescobrirAreaDialog();
				if(x<canvasColunas && y < canvasLinhas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							possui=true;
						}
					}
				}
				else if(x==canvasColunas && y==canvasLinhas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							possui=true;
						}
					}
				}
				else if(y==canvasLinhas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							possui=true;
						}
					}
				}
				else if(x==canvasColunas) {
					for(int i=0;i<2;i++) {
						for(int j=0;j<2;j++) {
							possui=true;
						}
					}
				}
				descobrirAreaDialog.setVisible(true);
			}
			
		}
	}

	public int getQuadradoWidth() {
		return quadradoWidth;
	}

	public void setQuadradoWidth(int quadradoWidth) {
		this.quadradoWidth = quadradoWidth;
	}

	public int getQuadradoHeight() {
		return quadradoHeight;
	}

	public void setQuadradoHeight(int quadradoHeight) {
		this.quadradoHeight = quadradoHeight;
	}

	public int getCanvasColunas() {
		return canvasColunas;
	}

	public void setCanvasColunas(int canvasColunas) {
		this.canvasColunas = canvasColunas;
	}

	public int getCanvasLinhas() {
		return canvasLinhas;
	}

	public void setCanvasLinhas(int canvasLinhas) {
		this.canvasLinhas = canvasLinhas;
	}

	public boolean[][] getMatrizExplosao() {
		return matrizExplosao;
	}

	public void setMatrizExplosao(boolean[][] matrizExplosao) {
		this.matrizExplosao = matrizExplosao;
	}

	public Jogador getJogador() {
		return jogador;
	}

	public static int getTotalEmbarcacoes() {
		return totalEmbarcacoes;
	}

	public static void setTotalEmbarcacoes(int totalEmbarcacoes) {
		Tabuleiro.totalEmbarcacoes = totalEmbarcacoes;
	}

	public Embarcacoes[][] getEmbarcacoes() {
		return embarcacoes;
	}

	public void setEmbarcacoes() {

		for (int i = 0; i < canvasLinhas; i++) {
			for (int j = 0; j < canvasColunas; j++) {
				this.embarcacoes[i][j] = new Embarcacoes();
			}
		}

		for (int i = 0; i < canvasLinhas; i++) {
			for (int j = 0; j < canvasColunas; j++) {

				if (!this.embarcacoes[i][j].isJaPassouCasa()) {
					switch (arquivo.getMatriz(i, j)) {
					case 1:
						this.embarcacoes[i][j].setPosicaoInicial(i, j);
						this.embarcacoes[i][j].setEmbarcacao(1);
						this.embarcacoes[i][j].setDirecaoHorizontal(false);
						break;
					case 2:

						if (j < canvasColunas - 1 && arquivo.getMatriz(i, j + 1) == 2) {

							for (int x = j; x < j + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[i][x].setDirecaoHorizontal(true);
								this.embarcacoes[i][x].setPosicaoInicial(i, j);
								this.embarcacoes[i][x].setEmbarcacao(2);
								this.embarcacoes[i][x].setJaPassouCasa(true);
							}
						} else {
							for (int x = i; x < i + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[x][j].setPosicaoInicial(i, j);
								this.embarcacoes[x][j].setEmbarcacao(2);
								this.embarcacoes[x][j].setJaPassouCasa(true);
							}
						}
						break;
					case 3:
						if (j < canvasColunas - 1 && arquivo.getMatriz(i, j + 1) == 3) {

							for (int x = j; x < j + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[i][x].setDirecaoHorizontal(true);
								this.embarcacoes[i][x].setPosicaoInicial(i, j);
								this.embarcacoes[i][x].setEmbarcacao(3);
								this.embarcacoes[i][x].setJaPassouCasa(true);
							}
						} else {
							for (int x = i; x < i + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[x][j].setPosicaoInicial(i, j);
								this.embarcacoes[x][j].setEmbarcacao(3);
								this.embarcacoes[x][j].setJaPassouCasa(true);
							}
						}

						break;
					case 4:
						if (j < canvasColunas - 1 && arquivo.getMatriz(i, j + 1) == 4) {

							for (int x = j; x < j + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[i][x].setDirecaoHorizontal(true);
								this.embarcacoes[i][x].setPosicaoInicial(i, j);
								this.embarcacoes[i][x].setEmbarcacao(4);
								this.embarcacoes[i][x].setJaPassouCasa(true);
							}
						} else {
							for (int x = i; x < i + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[x][j].setPosicaoInicial(i, j);
								this.embarcacoes[x][j].setEmbarcacao(4);
								this.embarcacoes[x][j].setJaPassouCasa(true);
							}
						}
						break;
					case 5:
						if (j < canvasColunas - 1 && arquivo.getMatriz(i, j + 1) == 5) {

							for (int x = j; x < j + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[i][x].setDirecaoHorizontal(true);
								this.embarcacoes[i][x].setPosicaoInicial(i, j);
								this.embarcacoes[i][x].setEmbarcacao(5);
								this.embarcacoes[i][x].setJaPassouCasa(true);
							}
						} else {
							for (int x = i; x < i + arquivo.getMatriz(i, j); x++) {
								this.embarcacoes[x][j].setPosicaoInicial(i, j);
								this.embarcacoes[x][j].setEmbarcacao(5);
								this.embarcacoes[x][j].setJaPassouCasa(true);
							}
						}
						break;
					default:
						this.embarcacoes[i][j].setPosicaoInicial(i, j);
						this.embarcacoes[i][j].setEmbarcacao(0);
						this.embarcacoes[i][j].setJaPassouCasa(true);
						break;
					}
				}
			}
		}
	}
}
