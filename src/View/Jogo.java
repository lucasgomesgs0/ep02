package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import Control.StatusMenuThread;
import Control.TabuleiroThread;
import Model.Arquivo;
import Model.Jogador;
import Model.Tabuleiro;

public class Jogo extends JFrame {
	
	private static Tabuleiro tabuleiro;
	static Jogador jogador;
	static TabuleiroThread updateScreenThread;
	static StatusMenuThread updateStatusMenuThread;
	static String nomeArquivo;
	
	public static void main(String[] args) {
		int chances = 0;
		
		Arquivo arquivo = new Arquivo();
		
		arquivo.lerArquivo(nomeArquivo);

		jogador = new Jogador();
		
		for (int i = 1; i <= 5; i++) {
			chances += i*arquivo.getEmbarcacoes().get(i);
		}

		jogador.setChances(chances+5);
		tabuleiro = new Tabuleiro(arquivo,jogador);
		updateScreenThread = new TabuleiroThread(tabuleiro);
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Jogo janela = new Jogo(true);
					janela.setVisible(true);
					tabuleiro.setMainFrame(janela);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});


	}

	public Jogo(boolean thread) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Centralizar janela
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		setTitle("EP02 - Batalha Naval");

		// Tamanho janela
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenWidht = screenSize.width;
		int screenHeight = screenSize.height;

		setSize(screenWidht + 2, screenHeight);
		setVisible(true);
		
		// Inicia Thread com timer para redesenhar a tela.
		if(thread) {
			updateScreenThread.start();
		}

		tabuleiro.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();

				int x_pos = x / (tabuleiro.getQuadradoWidth()+1);
				int y_pos = y / (tabuleiro.getQuadradoHeight()+1);
				
				if(x_pos!=0 && y_pos!=0) {
					tabuleiro.setShot(x_pos, y_pos);
				}

			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

		});

		// Janela
		JPanel status = new JPanel();
		status.setLayout(new GridLayout(10,1));
		
		Dimension size = new Dimension();
		size.width = 300;
		status.setPreferredSize(size);
		
		JToggleButton tglbtnAtaqueNormal = new JToggleButton("Ataque normal");
		status.add(tglbtnAtaqueNormal);

		JToggleButton tglbtnAtaqueLinha = new JToggleButton("Ataque especial na linha");
		status.add(tglbtnAtaqueLinha);
		
		JToggleButton tglbtnAtaqueHorizontal = new JToggleButton("Ataque especial na horizontal");
		status.add(tglbtnAtaqueHorizontal);
		
		JToggleButton tglbtnAtaqueArea2x2 = new JToggleButton("Ataque especial 2x2");
		status.add(tglbtnAtaqueArea2x2);
		
		JToggleButton tglbtnDescobrir2x2 = new JToggleButton("Descobrir área 2x2");
		status.add(tglbtnDescobrir2x2);
		
		ButtonGroup toggleGroup = new ButtonGroup();
		toggleGroup.add(tglbtnAtaqueNormal);
		toggleGroup.add(tglbtnAtaqueLinha);
		toggleGroup.add(tglbtnAtaqueHorizontal);
		toggleGroup.add(tglbtnAtaqueArea2x2);
		toggleGroup.add(tglbtnDescobrir2x2);
		
		tglbtnAtaqueNormal.setActionCommand("Ataque Normal");
		tglbtnAtaqueLinha.setActionCommand("Ataque Especial Linha");
		tglbtnAtaqueHorizontal.setActionCommand("Ataque Especial Coluna");
		tglbtnAtaqueArea2x2.setActionCommand("Ataque Especial 2x2");
		tglbtnDescobrir2x2.setActionCommand("Descobrir 2x2");
		
		tglbtnAtaqueNormal.addActionListener(new ToggleListener());
		tglbtnAtaqueLinha.addActionListener(new ToggleListener());
		tglbtnAtaqueHorizontal.addActionListener(new ToggleListener());
		tglbtnAtaqueArea2x2.addActionListener(new ToggleListener());
		tglbtnDescobrir2x2.addActionListener(new ToggleListener());
		
		updateStatusMenuThread = new StatusMenuThread(jogador, status);
		updateStatusMenuThread.start();
		
		getContentPane().add(status, BorderLayout.WEST);
		getContentPane().add(tabuleiro);

		setVisible(true);

	}
	
	private class ToggleListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			if (command.equals("Ataque Normal")) {
				Tabuleiro.jogada = 1;
			} else if (command.equals("Ataque Especial Linha")) {
				Tabuleiro.jogada = 2;
			} else if (command.equals("Ataque Especial Coluna")) {
				Tabuleiro.jogada = 3;
			} else if (command.equals("Ataque Especial 2x2")) {
				Tabuleiro.jogada = 4;
			} else if (command.equals("Descobrir 2x2")) {
				Tabuleiro.jogada = 5;
			}
		}
	}
}
