package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Font;

public class DescobrirAreaDialog extends JDialog {

	/**
	 * Create the dialog.
	 */
	public DescobrirAreaDialog() {
		setModal(true);
		setSize(500, 150);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JLabel lblPossuiEmbarcaoNessa = new JLabel("Possui embarcação nessa área!");
		lblPossuiEmbarcaoNessa.setFont(new Font("Dialog", Font.BOLD, 25));
		lblPossuiEmbarcaoNessa.setBounds(23, 12, 446, 44);
		getContentPane().add(lblPossuiEmbarcaoNessa);
		
		JButton btnOk = new JButton("OK");
		btnOk.setBounds(193, 80, 117, 25);
		getContentPane().add(btnOk);
		btnOk.addActionListener(new ButtonClickListener());
	
	}

	private class ButtonClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}
}
