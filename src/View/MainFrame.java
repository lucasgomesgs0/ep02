package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;

public class MainFrame extends JFrame {
	private ArrayList<String> arquivos = new ArrayList<String>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {
		arquivos = listarArquivos();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setTitle("EP02 - Batalha Naval");

		setSize(450, 390);
		setLocationRelativeTo(null);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel lblBatalhaNaval = new JLabel("Batalha Naval 2.0");
		lblBatalhaNaval.setFont(new Font("Dialog", Font.BOLD, 40));
		lblBatalhaNaval.setBounds(18, 14, 413, 117);
		getContentPane().add(lblBatalhaNaval);

		JButton btnJogar = new JButton("Jogar");
		btnJogar.setBounds(150, 140, 150, 25);
		getContentPane().add(btnJogar);

		JButton btnInstrucoes = new JButton("Instruções");
		btnInstrucoes.setBounds(150, 180, 150, 25);
		getContentPane().add(btnInstrucoes);

		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(150, 220, 150, 25);
		getContentPane().add(btnSair);

		btnJogar.setActionCommand("Jogar");
		btnInstrucoes.setActionCommand("Instrucoes");
		btnSair.setActionCommand("Sair");

		btnJogar.addActionListener(new ButtonClickListener());
		btnInstrucoes.addActionListener(new ButtonClickListener());
		btnSair.addActionListener(new ButtonClickListener());
	}
	public ArrayList<String> listarArquivos(){
		ArrayList<String> arquivosArray = new ArrayList<String>();
		String path = System.getProperty("user.dir") + "/maps/";
		File arquivos = new File(path);
		File[] listaArquivos = arquivos.listFiles();
		
		for(File arquivo : listaArquivos) {
			arquivosArray.add(arquivo.getName());
		}
		return arquivosArray;
	}
	
	private class ButtonClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String[] args = null;
			String command = e.getActionCommand();
			if (command.equals("Jogar")) {
				Random randomPosition = new Random();
				int position = randomPosition.nextInt(arquivos.size());
				Jogo.nomeArquivo = arquivos.get(position);
				dispose();
				Jogo.main(args);
			} else if (command.equals("Instrucoes")) {
				dispose();
				new Instrucoes().setVisible(true);

			} else if (command.equals("Sair")) {
				System.exit(0);
			}
		}
	}
}
