package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Instrucoes extends JFrame {

	/**
	 * Create the frame.
	 */
	public Instrucoes() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JLabel lblText = new JLabel();
		lblText.setFont(new Font("Dialog", Font.BOLD, 15));
		lblText.setBounds(12, 53, 768, 423);
		lblText.setText("<html><p>	Para auxiliar a encontrar todas as embarcações, o jogador pode fazer o uso de jogadas especias, mas elas diminuem da suas chances, são elas: </p>"
				+ "<ul> <li>Ataque normal(-1 chance): a jogada normal consiste em atacar apenas um ponto do tabuleiro.</li>"
				+ "<li>Ataque especial na linha/coluna(-8 chances): essa jogada consiste em atacar toda a linha/coluna do tabuleiro, assim derrubando todas as embarcações presentes.</li>"
				+ "<li>Ataque especial em uma área 2x2(-6 chances): essa jogada consiste em atacar uma área 2x2 do tabuleiro, assim derrubando todas as embarcações presentes.</li>"
				+ "<li>Descobrir embarcações em uma área 2x2(-3 chances): essa jogada consiste em verificar a existência de embarcações em uma área de 2x2 do tabuleiro.</li>"
				+ "</ul>"
				+ "<p>Há a possibilidade de não acertar nenhuma embarcação, atigindo assim a água. Ao atingir a água, irá mostrar um símbolo de explosão no local."
				+ "Ao acertar alguma parte de uma embarcação, o local selecionado irá escurer, mostrando que há uma embarcação ali.<br>"
				+ "A embarcação será revelada apenas quando todas as partes dela for atacada.<br><br>"
				+ "<br>Ao atingir o número de 0 chances significará que o jogador perdeu, podendo selecionar um novo mapa ou jogar novamente o mesmo. O jogador ganhará o"
				+ "jogo ao descobrir todas as embarcações presentes no tabuleiro."
				+ "</p></html>");
		getContentPane().add(lblText);
		
		JLabel lblNewLabel = new JLabel("Instruções");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 20));
		lblNewLabel.setBounds(342, 12, 136, 29);
		getContentPane().add(lblNewLabel);
		
		JButton btnVoltarAoMenu = new JButton("Voltar ao menu");
		btnVoltarAoMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				new MainFrame().setVisible(true);
			}
		});
		btnVoltarAoMenu.setBounds(330, 508, 148, 25);
		getContentPane().add(btnVoltarAoMenu);
	}

}
