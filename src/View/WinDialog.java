package View;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class WinDialog extends JDialog {
	/**
	 * Create the dialog.
	 */
	private JFrame frame;
	public WinDialog(JFrame frame) {
		this.frame = frame;
		
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(450,264);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);

		JLabel lblWin = new JLabel("Você ganhou!");
		lblWin.setFont(new Font("Dialog", Font.BOLD, 34));
		lblWin.setBounds(100, 24, 262, 58);
		getContentPane().add(lblWin);

		JButton btnVoltarProMenu = new JButton("Voltar para o menu");
		btnVoltarProMenu.setBounds(146, 145, 170, 25);
		getContentPane().add(btnVoltarProMenu);

		JButton btnJogarMesmoMapa = new JButton("Jogar de novo");
		btnJogarMesmoMapa.setBounds(146, 97, 170, 25);
		getContentPane().add(btnJogarMesmoMapa);

		btnJogarMesmoMapa.setActionCommand("Jogar de novo");
		btnVoltarProMenu.setActionCommand("Voltar para o menu");

		btnJogarMesmoMapa.addActionListener(new ButtonClickListener());
		btnVoltarProMenu.addActionListener(new ButtonClickListener());
	}

	private class ButtonClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String[] args = null;
			String command = e.getActionCommand();
			if (command.equals("Jogar de novo")) {
				dispose();
				frame.setVisible(false);
				Jogo.main(args);
			} else if (command.equals("Voltar para o menu")) {
				dispose();
				frame.setVisible(false);
				new MainFrame().setVisible(true);
			}
		}
	}
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
