package Teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Model.Jogador;

class JogadorTest {
	public int chances;
	public Jogador jogador;
	
	@BeforeEach
	public void beforeTests() {
		chances = 20;
		jogador = new Jogador();
	}
	
	@Test
	public void testaConstrutor() {
		jogador = new Jogador();

	}
	
	@Test
	public void testeJogadaNormal() {
		jogador.setChances(chances);

		assertEquals(chances-1, jogador.jogadaNormal());
	}
	
	@Test
	public void testeJogadaEspecialLinha() {
		jogador.setChances(chances);

		assertEquals(chances-8, jogador.jogadaEspecialLinha());
	}
	
	@Test
	public void testeJogadaEspecialColuna() {
		jogador.setChances(chances);

		assertEquals(chances-8, jogador.jogadaEspecialColuna());
	}
	
	@Test
	public void testeJogadaEspecialArea2x2() {
		jogador.setChances(chances);

		assertEquals(chances-6, jogador.jogadaEspecialArea2x2());
	}
	
	@Test
	public void testeDescobrirArea2x2() {
		jogador.setChances(chances);

		assertEquals(chances-3, jogador.jogadaDescobrirArea2x2());
	}
	
	@AfterEach
	public void afterTests() {
		jogador = null;
	}

}
