Aluno: Lucas Gomes Silva<br />
Matrícula: 16/0133505<br />
Disciplina: Orientação a Objetos<br />
IDE utilizada: Eclipse<br />

# EP2

#### Prepação para iniciar o jogo
Colocar os mapas a serem utilizados dentro da pasta **/map** na raiz do projeto.<br />
Foi utilizado a IDE Eclipse para o desenvolvimento do jogo, assim tendo a necessidade do mesmo para executar o jogo.<br />
Para iniciar o jogo bastar executar a classe **MainFrame.java** presente no pacote **View**.


#### Dentro do jogo

Após iniciar o jogo, terá três opções: Jogar, Instruções e Sair.<br />
__Jogar__ : O jogo selecionara de forma randômica um mapa presente na pasta do jogo e logo em seguida o tabuleiro aparecerá na tela.
__Instruções__ : Instruções sobre o jogo, onde é falado sobre as habilidades especiais e ícones presentes no tabuleiro.
__Sair__ : Sai do jogo


#### Possíveis bugs
Ao iniciar o jogo, pode acontecer uma leve demora para aparecer o tabuleiro ou a pontuação no lado esquerdo da tela. 
Outro bug que pode vim a aparecer é quando as embarcações começam a piscar.
